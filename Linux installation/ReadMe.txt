# INSTALLATION #
(1) sudo -H ./MyAppInstaller_web.install
-enter destination for program
-enter destination folder for MATLAB runtime (e.g. /usr/local/MATLAB/MATLAB_Runtime)
(2) set path to the MATLAB runtime
in terminal type (needs to be performed only once):
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH:+${LD_LIBRARY_PATH}:}\
/usr/local/MATLAB/MATLAB_Runtime/R2023a/runtime/glnxa64:\
/usr/local/MATLAB/MATLAB_Runtime/R2023a/bin/glnxa64:\
/usr/local/MATLAB/MATLAB_Runtime/R2023a/sys/os/glnxa64:\
/usr/local/MATLAB/MATLAB_Runtime/R2023a/extern/bin/glnxa64"
this is assuming runtime was saved at "/usr/local/MATLAB/MATLAB_Runtime/R2023a/" (else correct string accordingly)

# DATA.MAT FILE #
-This file can be placed anywhere on the computer (in a read-and-writable-folder). 
-When you start TYtool a dialouge window will ask you to locate the file (note: this window may end up behind the main window depending on screen size)
-If you manually add or delete reactions you will again be asked to locate the file via a dialouge window 

# RUN PROGRAM #
start terminal in application folder and type: 
./run_TYtool.sh /usr/local/MATLAB/MATLAB_Runtime/R2023a
(if your runtime is in a different location correct string accordingly)

# HELP #
Here is more info that may help if it does not work as expected: 
https://se.mathworks.com/help/compiler/install-deployed-application.html
https://se.mathworks.com/help/compiler_sdk/ml_code/mcr-path-settings-for-run-time-deployment.html
You can always email me too (but i'm pretty useless at linux; but got it working using above method):
daniel.thor##karolinska== (substitute ## --> @ and == --> .se)