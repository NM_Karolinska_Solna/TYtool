(1) Download and unzip.

(2) Run MyAppInstaller_web to install TYtool. It will automatically download needed files and create a stand-alone-application on your computer.

(3) The file "data.mat" must be in the same folder as the application

Here is more info that may help if it does not work as expected: https://se.mathworks.com/help/compiler/install-deployed-application.html
But the best thing is probably to email me at:
daniel.thor##karolinska== (substitute ## --> @ and == --> .se)